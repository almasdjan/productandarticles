package com.example.productsAndArticles.repo;

import com.example.productsAndArticles.models.Article;
import com.example.productsAndArticles.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface ArticleRepository extends JpaRepository<Article, Long> {
    Optional<Article> findByName(String name);

    Optional<Article> findByProductId(Product product);

    @Query("SELECT a FROM Article a WHERE " +
            "(:name is null or a.name = :name) AND" +
            "(:productId is null or a.productId = :productId) AND" +
            "(:content is null or a.content = :content) AND" +
            "(:createdDate is null or a.createdDate = :createdDate)")
    List<Article> findAllByParams(String name, Long productId, String content, LocalDate createdDate);


}
