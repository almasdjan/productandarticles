package com.example.productsAndArticles.repo;

import com.example.productsAndArticles.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Long> {

    Optional<Product> findByName(String name);

    Optional<Product> findByDescription(String description);

    @Query("SELECT p FROM Product p WHERE " +
            "(:name is null or p.name = :name) AND " +
            "(:description is null or p.description = :description) AND" +
            "(:price is null or p.price = :price)")
    List<Product> findAllByParams(String name, String description,Double price);



}
