package com.example.productsAndArticles.controllers;

import com.example.productsAndArticles.models.Article;
import com.example.productsAndArticles.models.Product;
import com.example.productsAndArticles.services.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping(path="api/articles")
public class ArticleController {
    private final ArticleService articleService;

    @Autowired
    public ArticleController(ArticleService articleService) {
        this.articleService = articleService;
    }

    @GetMapping
    public List<Article> getArticles(@RequestParam(required = false) String name,
                                     @RequestParam(required = false) Long productId,
                                     @RequestParam(required = false) String content,
                                     @RequestParam(required = false) LocalDate createdDate) {
        return articleService.getAll(name, productId, content, createdDate);
    }

    @PostMapping
    public void saveArticle(@RequestBody Article article) {
        articleService.addArticle(article);
    }


    @DeleteMapping(path = "/{articleId}")
    public void deleteArticle(@PathVariable("articleId")Long articleId ){
        articleService.deleteArticle(articleId);
    }


    @PutMapping(path ="/{articleId}")
    public void updateProduct(
            @PathVariable("articleId") Long articleId,
            @RequestParam(required = false) Long productId,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String content,
            @RequestParam(required = false) 
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
            LocalDate createdDate){
        articleService.updateArticle(articleId,productId,name,content,createdDate);
    }

}
