package com.example.productsAndArticles.controllers;

import com.example.productsAndArticles.models.Product;

import com.example.productsAndArticles.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping(path="api/products")
public class ProductController {
    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public List<Product> getProducts(@RequestParam(required = false) String name,
                                     @RequestParam(required = false) String description,
                                     @RequestParam(required = false) Double price)
    {
        return productService.getAll(name, description, price);
    }

    @PostMapping
    public void saveProduct(@RequestBody Product products) {
        productService.addProduct(products);
    }

    @DeleteMapping(path = "/{productId}")
    public void deleteProduct(@PathVariable("productId")Long productId ){
        productService.deleteProduct(productId);
    }



    @PutMapping(path ="/{productId}")
    public void updateProduct(
            @PathVariable("productId") Long productId,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String description,
            @RequestParam(required = false) Double price){
        productService.updateProduct(productId,name,description,price);
    }

}
