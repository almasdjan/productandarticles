package com.example.productsAndArticles.models;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name="article")
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;



    private Long productId;

    private String name;
    private String content;
    private LocalDate createdDate;


    public Article() {
    }

    public Article(Long id, Long productId, String name, String content, LocalDate createdDate) {
        this.id = id;
        this.productId = productId;
        this.name = name;
        this.content = content;
        this.createdDate = createdDate;
    }

    public Article(Long productId, String name, String content, LocalDate createdDate) {
        this.productId = productId;
        this.name = name;
        this.content = content;
        this.createdDate = createdDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }
}
