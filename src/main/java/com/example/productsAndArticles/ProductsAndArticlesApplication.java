package com.example.productsAndArticles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductsAndArticlesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductsAndArticlesApplication.class, args);
	}

}
