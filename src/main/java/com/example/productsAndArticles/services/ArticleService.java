package com.example.productsAndArticles.services;

import com.example.productsAndArticles.models.Article;
import com.example.productsAndArticles.repo.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


@Service
public class ArticleService {
    private final ArticleRepository articleRepository;

    @Autowired
    public ArticleService(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    public List<Article> getAll(String name, Long productId, String content, LocalDate createdDate) {
        return articleRepository.findAllByParams(name, productId, content,createdDate);
    }


    public void addArticle(Article article) {

        Optional<Article> optionalArticle = articleRepository.findByName(article.getName());
        if (optionalArticle.isPresent()){
            throw new IllegalArgumentException("such article exists");
        }
        articleRepository.save(article);
    }

    public void deleteArticle(Long articleId) {
        boolean exists = articleRepository.existsById(articleId);
        if (!exists) {
            throw new IllegalStateException("article with id " + articleId + " does not exists");
        }
        articleRepository.deleteById(articleId);
    }

    @Transactional
    public void updateArticle(Long articleId,Long productId, String name, String content, LocalDate createdDate) {
        Article article = articleRepository.findById(articleId)
                .orElseThrow(() -> new IllegalStateException(
                        "article with id " + articleId + " does not exists"));

        if (productId != null && !Objects.equals(article.getProductId(), productId)) {
            article.setProductId(productId);
        }

        if (name != null && name.length() > 0 && !Objects.equals(article.getName(), name)) {
            Optional<Article> optionalArticle = articleRepository.findByName(name);
            if (optionalArticle.isPresent()) {
                throw new IllegalStateException("name is taken");
            }
            article.setName(name);
        }

        if (content != null && content.length() > 0 && !Objects.equals(article.getContent(), content)) {
            article.setContent(content);
        }

        if (createdDate != null && !Objects.equals(article.getCreatedDate(), createdDate)) {
            article.setCreatedDate(createdDate);
        }

    }
}
