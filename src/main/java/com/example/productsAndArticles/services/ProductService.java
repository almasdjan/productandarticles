package com.example.productsAndArticles.services;

import com.example.productsAndArticles.models.Product;
import com.example.productsAndArticles.repo.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;


@Service
public class ProductService{

    private final ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    public List<Product> getAll(String name, String description,Double price) {

        return productRepository.findAllByParams(name,description,price);
    }

    public void addProduct(Product product) {

        Optional<Product> optionalProduct = productRepository.findByName(product.getName());
        if (optionalProduct.isPresent()){
            throw new IllegalArgumentException("such product exists");
        }
        productRepository.save(product);
    }

    public void deleteProduct(Long productId) {
        boolean exists = productRepository.existsById(productId);
        if (!exists) {
            throw new IllegalStateException("product with id " + productId + " does not exists");
        }
        productRepository.deleteById(productId);
    }


    @Transactional
    public void updateProduct(Long productId, String name, String description, Double price) {
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new IllegalStateException(
                        "product with id " + productId + " does not exists"));


        if (name != null && name.length() > 0 && !Objects.equals(product.getName(), name)) {
            Optional<Product> optionalProduct = productRepository.findByName(name);
            if (optionalProduct.isPresent()) {
                throw new IllegalStateException("name is taken");
            }
            product.setName(name);
        }

        if (description != null && description.length() > 0 && !Objects.equals(product.getDescription(), description)) {
            product.setDescription(description);
        }

        if (price != null && !Objects.equals(product.getPrice(), price)) {
            product.setPrice(price);
        }

    }
}