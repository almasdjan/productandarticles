create table product (
                         id bigserial primary key,
                         name varchar(255) unique,
                         description varchar(255),
                         price numeric
);

create table article(
                        id bigserial primary key,
                        product_id bigint references product(id),
                        name varchar(255) unique,
                        content text,
                        created_date date
);